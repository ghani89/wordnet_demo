package factory;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class Factory {

	private HashMap<String,Class> map ;
	
	
	

	public Factory() {
		super();
		map = new HashMap<String,Class>();
		try {
			map = (HashMap<String, Class>) ParseurXML.Parseur();
		} catch (ClassNotFoundException | ParserConfigurationException
				| SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Fonction getFonction(String nomFonction)
	{
		Class c = map.get(nomFonction);
		Fonction f = null;
		
			try {
				f = (Fonction) c.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		return f;
		
	}
	
	

	public HashMap<String, Class> getMap() {
		return map;
	}

	public void setMap(HashMap<String, Class> map) {
		this.map = map;
	}
	
	

}
