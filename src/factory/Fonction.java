package factory;

import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import edu.sussex.nlp.jws.JWS;

public interface Fonction {
	
	public double getScore(IndexWord word1 , IndexWord word2,JWS ws, String tag)throws JWNLException;

}
