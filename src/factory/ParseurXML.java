package factory;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParseurXML {
	


public static Map<String, Class> Parseur() throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException
	{
  Map<String, Class> map = new HashMap<String, Class>();
		
  String fichier="Factory.xml";
	    String cle = null;
	    String valeur=null;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder build=factory.newDocumentBuilder();
		
		Document document = build.parse(fichier);
		Element racine = document.getDocumentElement();
		NodeList bases =racine.getElementsByTagName("fonction");
		
		for(int i=0; i<bases.getLength(); i++)
		{
			Node base=bases.item(i);
			//elements enfant
			NodeList element = base.getChildNodes();
			
			for(int j=0; j<element.getLength(); j++){
				Node enfant=element.item(j);
				if(enfant.getNodeName().equals("nom-fonction"))
					cle=enfant.getTextContent();
					
				
				if(enfant.getNodeName().equals("class"))
					
					valeur=enfant.getTextContent();
				}
		
				map.put(cle, Class.forName(valeur));
			 }
		
	return map;
    }
}
