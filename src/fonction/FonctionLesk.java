package fonction;

import java.util.StringTokenizer;

import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.Synset;
import edu.sussex.nlp.jws.AdaptedLesk;
import edu.sussex.nlp.jws.JWS;
import factory.Fonction;

public class FonctionLesk implements Fonction{

/*
	@Override
	public double getScore(IndexWord word1, IndexWord word2, JWS ws, String tag) throws JWNLException {
		//AdaptedLesk lesk = ws.getAdaptedLesk();
		
		double score=0.0;
		
		Synset[] syns1 = word1.getSenses();	
		
		Synset[] syns2 = word2.getSenses();	
		
		String syn1 = null;
		String syn2 = null;
		
		double scoreMax=0.0;
		
		String glossaire1;
		String glossaire2;
		
		System.out.println(syns1.length);
		System.out.println(syns2.length);
		for (int i=0;i<syns1.length;i++){
			
			 glossaire1=word1.getSense(i+1).getGloss().replaceAll("\"", "").replaceAll(";", "");
			
			
			StringTokenizer st1 = new StringTokenizer(glossaire1);
			
			for (int j=0;j<syns2.length;j++){
				//scoreMax =lesk.lesk(word1.getLemma(),i+1, word2.getLemma(),j+1, tag);
				scoreMax=0;	
				 glossaire2=word2.getSense(j+1).getGloss().replaceAll("\"", "").replaceAll(";", "");
				StringTokenizer st2 = new StringTokenizer(glossaire2);
				
				
				System.out.println("glossaire1="+glossaire1);
				System.out.println("glossaire2="+glossaire2);
				
				
				for(int x=0;x<st1.countTokens();x++)
				{
					String m1 = st1.nextToken();
					
					for(int y=0;y<st2.countTokens();y++)
					{
						String m2 = st2.nextToken();
						System.out.println(m1+","+m2);
						if(m1.equals(m2))
							scoreMax++;
					}
					
				}
				
				
				
				System.out.println(scoreMax+","+score);
				if(scoreMax > score)
				{
					score=scoreMax;
					
					syn1=glossaire1;
					syn2=glossaire2;
				}	
			}
		}
		
		System.out.println("syn1="+syn1);
		System.out.println("syn2="+syn2);
		StringTokenizer s1 = new StringTokenizer(syn1);
		StringTokenizer s2 = new StringTokenizer(syn2);
		int x = s1.countTokens();
		int y = s2.countTokens();
		
		int n = (x<y)?x:y;
		
		System.out.println("x="+x+",y="+y+",score="+score);
		
		return score/n;
	}*/
	
	

	@Override
	public double getScore(IndexWord word1, IndexWord word2, JWS ws, String tag) throws JWNLException {

		double score=0.0;
		
		Synset[] syns1 = word1.getSenses();	
		
		Synset[] syns2 = word2.getSenses();
		
		double scoreMax=0.0;
		
		String glos1;
		String glos2;
		
		String syn1 = null;
		String syn2 = null;
		
		
		for(int i=0;i<syns1.length;i++)
		{
			for(int j=0;j<syns2.length;j++)
			{
				
				glos1=word1.getSense(i+1).getGloss().replaceAll("\"", "").replaceAll(";", "");
				glos2=word2.getSense(j+1).getGloss().replaceAll("\"", "").replaceAll(";", "");
				
				/*System.out.println("glossaire1="+glos1);
				System.out.println("glossaire2="+glos2);
				System.out.println("====================================================================");
				*/
				
				
				StringTokenizer st1 = new StringTokenizer(glos1);
				scoreMax=0;	
			
				
				while(st1.hasMoreTokens())
				{
					String m1 = st1.nextToken();
					
					StringTokenizer st2 = new StringTokenizer(glos2);
					while(st2.hasMoreTokens())
					{
						String m2 = st2.nextToken();
						//System.out.println("m1="+m1+",m2="+m2);
						
						if(m1.equals(m2))
						{
							scoreMax++;
							glos1.replace(m1,"");
							glos2.replace(m2,"");
						}
					
					}
				}
				
				if(scoreMax > score)
				{
					score=scoreMax;
					
					syn1=glos1;
					syn2=glos2;
				}	
					
				
			}
		}
		
		
		
		System.out.println("syn1="+syn1);
		System.out.println("syn2="+syn2);
		StringTokenizer s1 = new StringTokenizer(syn1);
		StringTokenizer s2 = new StringTokenizer(syn2);
		int x = s1.countTokens();
		int y = s2.countTokens();
		
		int n = (x<y)?x:y;
		n+=score;
		
		System.out.println("x="+x+",y="+y+",score="+score);
		
		return (score/n)*100;
	}
}
