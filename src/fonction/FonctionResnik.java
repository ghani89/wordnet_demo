package fonction;

import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.Synset;
import edu.sussex.nlp.jws.JWS;
import edu.sussex.nlp.jws.Resnik;
import factory.Fonction;

public class FonctionResnik implements Fonction {

	

	@Override
	public double getScore(IndexWord word1, IndexWord word2, JWS ws, String tag) throws JWNLException {
		Resnik res = ws.getResnik();
		
		double score=0.0;
		
		Synset[] syns1 = word1.getSenses();	
		
		Synset[] syns2 = word2.getSenses();	
		
		for (int i=0;i<syns1.length;i++){
			
			double scoreMax=0.0;
			for (int j=0;j<syns2.length;j++){
				scoreMax =res.res(word1.getLemma(),i+1, word2.getLemma(),j+1, tag);
				
				if(scoreMax>score)
					score=scoreMax;
			}
		}
		return (score*100)/12.515034195374447;
	}

}
