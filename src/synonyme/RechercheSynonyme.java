package synonyme;

import java.io.FileInputStream;
import java.io.IOException;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.data.Word;
import net.didion.jwnl.dictionary.Dictionary;
import net.didion.jwnl.data.POS;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.sussex.nlp.jws.JWS;
import factory.Factory;
import factory.Fonction;

public class RechercheSynonyme {
	
private MaxentTagger tagger;	
private Dictionary dict;
private String dir="D:\\Program Files (x86)\\WordNet"; //chemin vers le dossier de wordnet sur le disque
private JWS	ws ;
public RechercheSynonyme() throws ClassNotFoundException, IOException, JWNLException {
		super();
		tagger = new MaxentTagger("tagger/wsj-0-18-left3words.tagger");
		JWNL.initialize(new FileInputStream("file_properties.xml"));
		 dict = Dictionary.getInstance();
		 ws = new JWS(dir, "2.1");
	}

public String getTagger(POS pos){
	String tag="";
	 if(pos.equals(POS.NOUN)) tag="n";
		else 
			 if(pos.equals(POS.VERB)) tag="v";
			else 
				 if(pos.equals(POS.ADJECTIVE)) tag="r";
				else 
					 if(pos.equals(POS.ADVERB)) tag="a";

	return tag;
	
}
public POS getTaggerMot(String mot){
		
		 String[] result = tagger.tagString(mot).split("/");
		 
		 String p=result[1];
		
		 POS pos=null;
		 
		 if(p.startsWith("NN")) pos=POS.NOUN;
			else 
				if(p.startsWith("VB")) pos=POS.VERB;
				else 
					if(p.startsWith("JJ")) pos=POS.ADJECTIVE;
					else 
						if(p.startsWith("RB") || p.equals("WRB"))pos=POS.ADVERB;
		
		return pos;

}

public boolean isSynonymDirect (IndexWord word1, IndexWord word2) throws JWNLException{
	
	//Recuperer les mot 1 et 2 depuis WordNet

		
	String mot=word2.getLemma().replaceAll(" ", "_");
		
		Synset[] syns = word1.getSenses();	
		
		
		boolean trouve=false;
		
		for (int ss = 0; ss < syns.length; ss++) 
		{
			Word[] mots=syns[ss].getWords();
			
			for (int j=0; j<mots.length;j++)
				 
				if(mots[j].getLemma().equals(mot))
				{
					trouve=true;
					break;
				}
		}
		
		return trouve;
		
	}

public boolean isIdentique (String mot1, String mot2){
	
	boolean trouve=false;
	if(mot1.equals(mot2))
		trouve=true;
	
	return trouve;
	
}

public Resultat recherche(String mot1, String mot2, String nomFct, double seuil) throws JWNLException {
	
	Resultat res = new Resultat();
	POS pos=null;
	if (getTaggerMot(mot1).equals(getTaggerMot(mot2)))
		pos=getTaggerMot(mot1);
	else
		pos=POS.NOUN;
	
	String tag=getTagger(pos);
	IndexWord word1 = dict.lookupIndexWord(pos,mot1);
    IndexWord word2 = dict.lookupIndexWord(pos,mot2);
	
   
    
    if(word1!=null && word2!=null) {
				
			if (isIdentique(mot1, mot2))
			{
				res.setMot1(mot1);
				res.setMot2(mot2);
				res.setRemarque("identique");
				
			}	
			else
				if(isSynonymDirect(word1,word2))
				{
					res.setMot1(mot1);
					res.setMot2(mot2);
					res.setRemarque("synonyme");
					 
				}	 
				else{
					
					//Calcul de similarité
					
					
						
					//factory
					Factory factory = new Factory();
					Fonction fonc = factory.getFonction(nomFct);
					
					double score=fonc.getScore(word1,  word2, ws,tag);
					if (score>=seuil)
					{
						res.setMot1(mot1);
						res.setMot2(mot2);
						res.setRemarque("simlaire");
						res.setScore(score);
						
					}
					else
					{
						res.setMot1(mot1);
						res.setMot2(mot2);
						res.setRemarque("simlaire<");
						res.setScore(score);
					}
				}
	
	}else
	{
		if(word1 == null)
		{
			res.setMot1(mot1);
			res.setRemarque("orphelin");
		}
		
		if(word2 == null)
		{
			res.setMot2(mot2);
			res.setRemarque("orphelin");
		}
	}
			

    
    return res;
    
    
}
}