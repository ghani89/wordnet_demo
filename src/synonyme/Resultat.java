package synonyme;

public class Resultat {
	
	private String mot1;
	private String mot2;
	private String remarque;
	private double score;
	
	
	
	public Resultat() {
		super();
		mot1 = null ;
		mot2 = null ; 
		remarque = null ; 
		score = -1;
	}
	
	public String getMot1() {
		return mot1;
	}
	public void setMot1(String mot1) {
		this.mot1 = mot1;
	}
	public String getMot2() {
		return mot2;
	}
	public void setMot2(String mot2) {
		this.mot2 = mot2;
	}
	public String getRemarque() {
		return remarque;
	}
	public void setRemarque(String remarque) {
		this.remarque = remarque;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	
	

}
