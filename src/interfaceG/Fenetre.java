package interfaceG;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.xml.parsers.ParserConfigurationException;

import net.didion.jwnl.JWNLException;

import org.xml.sax.SAXException;

import synonyme.RechercheSynonyme;
import synonyme.Resultat;
import factory.ParseurXML;

public class Fenetre extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4865601895918132972L;
	
	private ArrayList<JTextField> mots1;
	private ArrayList<JTextField> mots2;
	
	private JPanel schema1;
	private JPanel schema2;
	
	private JButton ajouter1;
	private JButton ajouter2;
	
	private JButton effacer1;
	private JButton effacer2;
	
	private JButton valider;
	private JButton comparer;
	
	private JComboBox fonctions;
	
	private JTextField facteur;
	private JTextPane affichage; 
	private RechercheSynonyme s = null;

	public Fenetre() throws HeadlessException {
		super();
		init();
	}

	public Fenetre(String arg0) throws HeadlessException {
		super(arg0);
		init();
	}
	
	
	private void init()
	{
		try {
			s = new  RechercheSynonyme ();
		} catch (ClassNotFoundException | IOException | JWNLException e1) {
			e1.printStackTrace();
		}
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		setMinimumSize(new Dimension(750, 480));
		setDefaultLookAndFeelDecorated(true);
		//setExtendedState(this.MAXIMIZED_BOTH);
		
		setLocationRelativeTo(null);
		
		mots1 = new ArrayList<JTextField>();
		mots2 = new ArrayList<JTextField>();
		
		schema1 = new JPanel();
		schema1.setLayout(new FlowLayout());
		
		JScrollPane js1 = new JScrollPane(schema1);
		
		schema2 = new JPanel();
		schema2.setLayout(new FlowLayout());
		JScrollPane js2 = new JScrollPane(schema2);
		
		ajouter1 = new JButton("Ajouter");
		ajouter1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				ajouterChampsShema1(e);
				
			}
		});
		
		
		ajouter2 = new JButton("Ajouter");
		ajouter2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				ajouterChampsShema2(e);
			}
		});
		
		
		effacer1 = new JButton("Effacer");
		effacer1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				effacerSchema1(e);
			}
		});
		
		effacer2 = new JButton("Effacer");
		effacer2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				effacerSchema2(e);
			}
		});
		
		JPanel content  = new JPanel();
		
		content.setLayout(null);
		
		
		content.add(js1);
		content.add(ajouter1);
		
	
		content.add(js2);
		content.add(ajouter2);
		
		content.add(effacer1);
		content.add(effacer2);
		
		Insets insets = content.getInsets();
		
		js1.setBounds(25 + insets.left, 5 + insets.top,
		             600,70 );
		ajouter1.setBounds(25 + insets.left + 610, 5 + insets.top + 10,
	             ajouter1.getPreferredSize().width, ajouter1.getPreferredSize().height);
		
		effacer1.setBounds(25 + insets.left + 610, 5 + insets.top + 10 + ajouter1.getHeight(),
				effacer1.getPreferredSize().width, effacer1.getPreferredSize().height);
		effacer1.setEnabled(false);
		
		js2.setBounds(25 + insets.left, 5 + insets.top+80,
	             600, 70);
		
		ajouter2.setBounds(25 + insets.left + 610, 5 + insets.top+80 +10,
	             ajouter2.getPreferredSize().width, ajouter2.getPreferredSize().height);
		
		effacer2.setBounds(25 + insets.left + 610, 5 + insets.top + 90 + ajouter2.getHeight(),
				effacer2.getPreferredSize().width, effacer2.getPreferredSize().height);
	    
		effacer2.setEnabled(false);
		
		
		try {
			Set<String> fs = ParseurXML.Parseur().keySet();
			fonctions = new JComboBox<>(fs.toArray());
			
		} catch (ClassNotFoundException | ParserConfigurationException
				| SAXException | IOException e2) {
			e2.printStackTrace();
		} 
		
		
		JLabel fctsLabel = new JLabel("Fonctions:\n");
	   	content.add(fctsLabel);
	    content.add(fonctions);
	    
	    fctsLabel.setBounds(25 + insets.left , js2.getY()+js2.getHeight()+10,
	    		fctsLabel.getPreferredSize().width, fctsLabel.getPreferredSize().height);
	    
	    fonctions.setBounds(25 + insets.left , js2.getY()+js2.getHeight()+10+fctsLabel.getHeight(),
	    		fonctions.getPreferredSize().width, fonctions.getPreferredSize().height);
	    
	    
	    TitledBorder border1 = BorderFactory.createTitledBorder("Sch�ma 1");
	    TitledBorder border2 = BorderFactory.createTitledBorder("Sch�ma 2");
	    
	    js1.setBorder(border1);
	    js2.setBorder(border2);
	    
	    
	    
	    facteur = new JTextField(10);
	    
	    TitledBorder border3 = BorderFactory.createTitledBorder("Seuil   %");
	    facteur.setBorder(border3);
	    content.add(facteur);
	    facteur.setBounds(25 + insets.left + fonctions.getWidth() + 10, js2.getY()+js2.getHeight()+10,
	    		facteur.getPreferredSize().width, facteur.getPreferredSize().height);
	    
	    
	    valider = new JButton("Valider");
	    content.add(valider);
	    valider.setBounds(25 + insets.left + 610, js2.getY()+js2.getHeight()+20 ,
	    		valider.getPreferredSize().width, valider.getPreferredSize().height);
	    valider.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				recherche(e);
				
			}
		});
	    
	    comparer = new JButton("Comparer");
	    content.add(comparer);
	   /* comparer.setBounds(25 + insets.left + 610, js2.getY()+js2.getHeight()+20+valider.getHeight() ,
	    		comparer.getPreferredSize().width, comparer.getPreferredSize().height);
	    */
	    
	    comparer.setBounds(25 + insets.left , fonctions.getY()+fonctions.getHeight()+10,
	    		comparer.getPreferredSize().width, comparer.getPreferredSize().height);
	    
	    comparer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				comparer(e);
				
			}
		});
	 
	   
	   
	    
	    affichage = new JTextPane();
	    affichage.setEditable(false);
	    JScrollPane jcp = new JScrollPane(affichage);
	    
	    JSplitPane jsp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, content, jcp);
	    jsp.setDividerLocation(getHeight()/2);
	    jsp.setOneTouchExpandable(true);
	    getContentPane().setLayout(new BorderLayout());
	    getContentPane().add(jsp, BorderLayout.CENTER);
	    
	    
	}

	protected void recherche(ActionEvent e) {
		
		
		/*Iterator<JTextField> it1 =  mots1.iterator();
		Iterator<JTextField> it2 =  mots2.iterator();
		
		while (it1.hasNext())
		{
			JTextField t = it1.next();
			String s = t.getText().trim();
			if(s.isEmpty())
			{
				it1.remove();
			}
		}
		
		while (it2.hasNext())
		{
			JTextField t = it2.next();
			String s = t.getText().trim();
			if(s.isEmpty())
			{
				it2.remove();
			}
		}*/
		
		
		try {
			affichage.getDocument().remove(0, affichage.getDocument().getLength());
		} catch (BadLocationException e2) {
			e2.printStackTrace();
		}
		
		if(mots1.size() == 0 || mots2.size() == 0)
		{
			try {
				affichage.getDocument().insertString(affichage.getDocument().getEndPosition().getOffset(), "Les deux sch�mas sont differents\n", null);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			return;
		}
		
		String fonction = (String) fonctions.getSelectedItem();
		
		Style defaut = affichage.getStyle("default");
		StyleConstants.setFontSize(defaut, 16);
		
		Style style4 = affichage.addStyle("style4", defaut);
	      StyleConstants.setFontFamily(style4, "Arial");
	      StyleConstants.setBold(style4, true);
	      StyleConstants.setFontSize(style4, 18);
	      
		Style style1 = affichage.addStyle("style1", defaut);
		      StyleConstants.setFontFamily(style1,  "Arial");
		      StyleConstants.setBold(style1, true);
		      StyleConstants.setFontSize(style1, 16);
		Style style2 = affichage.addStyle("style2", style1);
		      StyleConstants.setForeground(style2, Color.RED);
		      StyleConstants.setFontSize(style2, 16);
		      
		Style style3 = affichage.addStyle("style3", style1);
		      StyleConstants.setForeground(style3, Color.BLUE);
		      StyleConstants.setFontSize(style3, 16);
		
		      
		Document d = affichage.getDocument();
		
		try {
			d.insertString(d.getEndPosition().getOffset(),fonction+":\n\n", style4);
		} catch (BadLocationException e2) {
			e2.printStackTrace();
		}
		
		for(int i = 0;i<mots1.size();i++)
		{
			if(mots1.get(i).getText().trim().isEmpty())
				continue;
			
			for(int j=0;j<mots2.size();j++)
			{	
			 
				if( mots2.get(j).getText().trim().isEmpty())
					continue;
				
			try {
				double seuil = 0.0;
				String f = facteur.getText().trim();
				
				if(!f.isEmpty())
					seuil = Double.valueOf(f).doubleValue();
				
				//affichages
				
								      
				      
				      
				Resultat r = s.recherche(mots1.get(i).getText(), mots2.get(j).getText(),fonction, seuil);
				
				
				if(r.getRemarque().equals("identique"))
				{
					d.insertString(d.getEndPosition().getOffset(),r.getMot1(), style1);
					d.insertString(d.getEndPosition().getOffset()," et ", defaut);
					d.insertString(d.getEndPosition().getOffset(),r.getMot2(), style1);
					d.insertString(d.getEndPosition().getOffset()," sont identique\n", style2);
					
				}
				else if(r.getRemarque().equals("synonyme"))
				{
					d.insertString(d.getEndPosition().getOffset(),r.getMot1(), style1);
					d.insertString(d.getEndPosition().getOffset()," et ", defaut);
					d.insertString(d.getEndPosition().getOffset(),r.getMot2(), style1);
					d.insertString(d.getEndPosition().getOffset()," sont des ", defaut);
					d.insertString(d.getEndPosition().getOffset(),"synonymes directe\n", style2);
					
				}
				else if(r.getRemarque().equals("simlaire"))
				{
					d.insertString(d.getEndPosition().getOffset(),r.getMot1(), style1);
					d.insertString(d.getEndPosition().getOffset()," et ", defaut);
					d.insertString(d.getEndPosition().getOffset(),r.getMot2(), style1);
					d.insertString(d.getEndPosition().getOffset()," sont ", defaut);
					d.insertString(d.getEndPosition().getOffset(),"similaires", style2);
					d.insertString(d.getEndPosition().getOffset()," � ", defaut);
					d.insertString(d.getEndPosition().getOffset(),(int)r.getScore()+"%\n", style3);
				}
				else if(r.getRemarque().equals("simlaire<"))
				{
					/*d.insertString(d.getEndPosition().getOffset(),r.getMot1(), style1);
					d.insertString(d.getEndPosition().getOffset()," et ", defaut);
					d.insertString(d.getEndPosition().getOffset(),r.getMot2(), style1);
					d.insertString(d.getEndPosition().getOffset()," ne sont pas ", defaut);
					d.insertString(d.getEndPosition().getOffset(),"simlaires", style2);
					d.insertString(d.getEndPosition().getOffset()," � ", defaut);
					d.insertString(d.getEndPosition().getOffset(),(int)seuil+"%\n", style3);*/
				}
				else if(r.getRemarque().equals("orphelin"))
				{
					if(r.getMot1()!=null)
					{
						d.insertString(d.getEndPosition().getOffset(),r.getMot1()+" ", style1);
						d.insertString(d.getEndPosition().getOffset(),"est un mot ", defaut);
						d.insertString(d.getEndPosition().getOffset(),"orphelin\n", style2);
					}
					if(r.getMot2()!=null)
					{
						d.insertString(d.getEndPosition().getOffset(),r.getMot2()+" ", style1);
						d.insertString(d.getEndPosition().getOffset(),"est un mot ", defaut);
						d.insertString(d.getEndPosition().getOffset(),"orphelin\n", style2);
					}
					break;
				}
				
				
				
				
				
				
			
			} catch (JWNLException e1) {
				
				e1.printStackTrace();
			} catch (BadLocationException e1) {
				
				e1.printStackTrace();
			}
			
			}
				
		}		 
	}

	protected void effacerSchema1(ActionEvent e) {
		
			schema1.remove( schema1.getComponentCount()-1);
		
			
			mots1.remove(mots1.size()-1);
			if(mots1.size()==0)
				effacer1.setEnabled(false);
			
			revalidate();
			repaint();
		
	}

	
	protected void effacerSchema2(ActionEvent e) {
		
		
		schema2.remove( schema2.getComponentCount()-1);
		
		
		mots2.remove(mots2.size()-1);
		if(mots2.size()==0)
			effacer2.setEnabled(false);
		
		revalidate();
		repaint();
	
	}
	
	protected void ajouterChampsShema1(ActionEvent e) {
		
		JTextField text = new JTextField(20);
		schema1.add(text);
		mots1.add(text);
		effacer1.setEnabled(true);
		validate();
	}
	
	protected void ajouterChampsShema2(ActionEvent e) {
		
		JTextField text = new JTextField(20);
		schema2.add(text);
		mots2.add(text);
		effacer2.setEnabled(true);
		validate();
	}
	
	protected void comparer(ActionEvent e)
	{
		
		
		TreeMap<Integer, String> map = new TreeMap<Integer,String>();
		
		try {
			affichage.getDocument().remove(0, affichage.getDocument().getLength());
		} catch (BadLocationException e2) {
			e2.printStackTrace();
		}
		
		if(mots1.size() == 0 || mots2.size() == 0)
		{
			try {
				affichage.getDocument().insertString(affichage.getDocument().getEndPosition().getOffset(), "Les deux sch�mas sont differents\n", null);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			return;
		}
		
		
		Style defaut = affichage.getStyle("default");
		StyleConstants.setFontSize(defaut, 16);
		
		Style style4 = affichage.addStyle("style4", defaut);
	      StyleConstants.setFontFamily(style4, "Arial");
	      StyleConstants.setBold(style4, true);
	      StyleConstants.setFontSize(style4, 18);
	      
		Style style1 = affichage.addStyle("style1", defaut);
		      StyleConstants.setFontFamily(style1,  "Arial");
		      StyleConstants.setBold(style1, true);
		      StyleConstants.setFontSize(style1, 16);
		Style style2 = affichage.addStyle("style2", style1);
		      StyleConstants.setForeground(style2, Color.RED);
		      StyleConstants.setFontSize(style2, 16);
		      
		Style style3 = affichage.addStyle("style3", style1);
		      StyleConstants.setForeground(style3, Color.BLUE);
		      StyleConstants.setFontSize(style3, 16);
		      
		Document d = affichage.getDocument();
		
		
		for(int i = 0;i<mots1.size();i++)
		{
			if(mots1.get(i).getText().trim().isEmpty())
				continue;
			
			for(int j=0;j<mots2.size();j++)
			{	
				if( mots2.get(j).getText().trim().isEmpty())
					continue;
				
			 
			try {
				double seuil = 0.0;
				String f = facteur.getText().trim();
				
				if(!f.isEmpty())
					seuil = Double.valueOf(f).doubleValue();
				
			
				map.clear();
				Resultat r = s.recherche(mots1.get(i).getText(), mots2.get(j).getText(),(String)fonctions.getItemAt(0), seuil);
				if(r.getRemarque().equals("simlaire"))
				{
					
					for(int k=0;k<fonctions.getItemCount();k++)
					{
						 r = s.recherche(mots1.get(i).getText(), mots2.get(j).getText(),(String)fonctions.getItemAt(k), seuil);
						 map.put((int)r.getScore(), (String)fonctions.getItemAt(k));
					}
					d.insertString(d.getEndPosition().getOffset(),"La meilleure distance s�mantique entre ", defaut);
					d.insertString(d.getEndPosition().getOffset(),r.getMot1(), style1);
					d.insertString(d.getEndPosition().getOffset()," et ", defaut);
					d.insertString(d.getEndPosition().getOffset(),r.getMot2(), style1);
					d.insertString(d.getEndPosition().getOffset()," est ", defaut);
					d.insertString(d.getEndPosition().getOffset(),map.lastKey()+"%", style3);
					d.insertString(d.getEndPosition().getOffset()," obtenu par la fonction ", defaut);
					d.insertString(d.getEndPosition().getOffset(),map.get(map.lastKey())+"\n", style2);
					
					
				}
				
	
			} catch (JWNLException e1) {
				e1.printStackTrace();
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			
			}
				
		}	
	}
	

}
